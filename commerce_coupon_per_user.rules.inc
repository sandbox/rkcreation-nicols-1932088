<?php

/**
 * @file
 * Coupon rules integration file.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_coupon_per_user_rules_action_info() {
	$actions = array();

	$actions['commerce_coupon_per_user_action_get_coupon_uses'] = array(
		'label' => t('Get the redemption number of a coupon for a user'),
		'parameter' => array(
			'account' => array(
				'type' => 'user',
				'label' => t('User'),
			),
			'commerce_coupon' => array(
				'type' => 'commerce_coupon',
				'label' => t('Commerce Coupon'),
			),
		),
		'group' => t('Commerce Coupon'),
		'provides' => array(
			'number_of_uses_for_user' => array(
				'type' => 'integer',
				'label' => t('number of uses for a user'),
			),
		),
	);

	return $actions;
}

/**
 * Action to get all uses for a coupon.
 */
function commerce_coupon_per_user_action_get_coupon_uses($account, $coupon) {
	return array('number_of_uses_for_user' => commerce_coupon_get_number_of_uses_for_user($coupon->coupon_id, $account));
}