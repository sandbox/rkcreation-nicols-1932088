<?php

/*
 * @file
 * Default coupon rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_coupon_per_user_default_rules_configuration_alter(&$configs) {
	$rule = &$configs['commerce_coupon_validate_uses_of_coupon'];
	$rule->condition(rules_condition(
					'data_is', array(
				'data:select' => 'coupon:commerce-coupon-number-of-uses',
				'op' => '==',
				'value' => 0,
					)
			)->negate());
}

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_coupon_per_user_default_rules_configuration() {
	$rules = array();

	// Add validation rule component to check the number of uses of a coupon
	$rule = rule(array(
		'account' => array(
			'type' => 'user',
			'label' => t('User'),
		),
		'coupon' => array(
			'type' => 'commerce_coupon',
			'label' => t('Coupon'),
		),
		'number_of_redemptions' => array(
			'type' => 'integer',
			'label' => t('Number Of Redemptions'),
		),
	));
	$rule->label = t('Coupon Validation: Set the coupon as invalid if number of uses is reached for this user');
	$rule
			->condition(
					'entity_has_field', array(
				'entity:select' => 'coupon',
				'field' => 'commerce_coupon_per_user_check',
			))
			->condition(
					'entity_has_field', array(
				'entity:select' => 'coupon',
				'field' => 'commerce_coupon_per_user_uses',
			))
			->condition(
					'entity_has_field', array(
				'entity:select' => 'coupon',
				'field' => 'commerce_coupon_per_user_allowed',
			))
			->condition(rules_or()
					->condition(
							'data_is',
							array(
								'data:select' => 'coupon:commerce-coupon-per-user-uses',
								'op' => '<',
								'value:select' => 'number-of-redemptions')
						)
					->condition(
							'data_is',
							array(
								'data:select' => 'coupon:commerce-coupon-per-user-uses',
								'op' => '==',
								'value:select' => 'number-of-redemptions')
						)
					->condition(
						rules_and()
							->condition(
								rules_condition(
										'data_is_empty', array(
									'data:select' => 'coupon:commerce-coupon-per-user-allowed',
								))->negate()
							)
							->condition(
								rules_condition(
										'list_contains', array(
									'list:select' => 'coupon:commerce-coupon-per-user-allowed',
									'item:select' => 'account'
								))->negate()
							)
					)
	);
	$rule->action('drupal_message', array(
		'message' => t('Sorry, the maximum number of redemptions for this coupon has been reached.'),
		'type' => 'error',
	));
	$rule->action('commerce_coupon_action_is_invalid_coupon', array());
	$rules['commerce_coupon_per_user_validate_uses_of_coupon_component'] = $rule;



	// Reaction on the validation event for check uses of coupons:
	$rule = rules_reaction_rule();
	$rule->label = t('Coupon Validation: Check the number of redemptions for a user');
	$rule->active = TRUE;

	$rule
			->event('commerce_coupon_validate')
			->condition(
					'entity_has_field', array(
				'entity:select' => 'coupon',
				'field' => 'commerce_coupon_per_user_check',
			))
			->condition(
					'data_is', array(
				'data:select' => 'coupon:commerce-coupon-per-user-check',
				'op' => '==',
				'value' => 1,
			))
			->condition(
					'entity_has_field', array(
				'entity:select' => 'coupon',
				'field' => 'commerce_coupon_per_user_uses',
			))
			->condition(rules_condition(
							'data_is', array(
						'data:select' => 'coupon:commerce-coupon-per-user-uses',
						'op' => '==',
						'value' => 0,
							)
					)->negate()
			)
			->condition(
					'entity_has_field', array(
				'entity:select' => 'coupon',
				'field' => 'commerce_coupon_per_user_allowed',
			))
			->action(
					'commerce_coupon_per_user_action_get_coupon_uses', array(
				'account:select' => 'commerce-order:owner',
				'commerce_coupon:select' => 'coupon',
			))
			->action(
					'component_commerce_coupon_per_user_validate_uses_of_coupon_component', array(
				'account:select' => 'commerce-order:owner',
				'coupon:select' => 'coupon',
				'number_of_redemptions:select' => 'number-of-uses-for-user',
	));
	$rules['commerce_coupon_per_user_validate_uses_of_coupon'] = $rule;

	return $rules;
}
